package com.cargocrew.cargoapp.models;

import java.io.Serializable;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Miki on 29.05.2017.
 */
@Data
public class TruckItem extends TransportationItem {

    private String date;

    private String type;

}
