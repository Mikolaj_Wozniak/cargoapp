package com.cargocrew.cargoapp;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.cargocrew.cargoapp.activities.MapsActivity;
import com.cargocrew.cargoapp.models.TransportationItem;
import com.google.firebase.auth.FirebaseAuth;

import static com.cargocrew.cargoapp.activities.MapsActivity.mContext;
import static java.security.AccessController.getContext;

/**
 * Created by Miki on 08.06.2017.
 */

public final class Utils {

    public static void setUserIDOnItem(TransportationItem truckItem) {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        String userID = auth.getCurrentUser().getUid();
        truckItem.setOwner(userID);
    }

    public static void showButtonForOwner(TransportationItem item, Button deleteButton) {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        String userID = auth.getCurrentUser().getUid();
        if (item.getOwner().equals(userID))
            deleteButton.setVisibility(View.VISIBLE);
    }

    public static Double zeroIfEditTextIsEmpty(EditText field) {
        if (field.getText().toString().isEmpty()) {
            return 0.0;
        }
        return Double.valueOf(field.getText().toString());
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
