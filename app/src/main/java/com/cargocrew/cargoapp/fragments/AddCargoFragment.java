package com.cargocrew.cargoapp.fragments;

import android.app.Fragment;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.cargocrew.cargoapp.R;
import com.cargocrew.cargoapp.activities.MapsActivity;
import com.cargocrew.cargoapp.models.CargoItem;
import com.cargocrew.cargoapp.models.Point;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.cargocrew.cargoapp.Utils.setUserIDOnItem;
import static com.cargocrew.cargoapp.Utils.zeroIfEditTextIsEmpty;
import static com.cargocrew.cargoapp.activities.MapsActivity.cargoRef;
import static com.cargocrew.cargoapp.activities.MapsActivity.currentRouteMarkerList;
import static com.cargocrew.cargoapp.activities.MapsActivity.truckRef;

/**
 * Created by Miki on 08.06.2017.
 */

public class AddCargoFragment extends Fragment {

    @BindView(R.id.addCargoBar)
    protected LinearLayout addCargoBar;

    @BindView(R.id.cargoDetailCountryEditText)
    protected EditText cargoDetailCountryEditText;

    @BindView(R.id.cargoDetailDestEditText)
    protected EditText cargoDetailDestEditText;

    @BindView(R.id.cargoDetailEstimEditText)
    protected EditText cargoDetailEstimateEditText;

    @BindView(R.id.cargoDetailHeightEditText)
    protected EditText cargoDetailHeightEditText;

    @BindView(R.id.cargoDetailLengthEditText)
    protected EditText cargoDetailLengthEditText;

    @BindView(R.id.cargoDetailPhoneNumberEditText)
    protected EditText cargoDetailPhoneNumberEditText;

    @BindView(R.id.cargoDetailWeightEditText)
    protected EditText cargoDetailWeightEditText;

    @BindView(R.id.cargoDetailWidthEditText)
    protected EditText cargoDetailWidthEditText;

    @BindView(R.id.cargoDetailZipCodeEditText)
    protected EditText cargoDetailZipCodeEditText;

    @BindView(R.id.additionalDetailsEditText)
    protected EditText additionalDetailsEditText;

    @BindView(R.id.cargoDetailCancelButtonn)
    protected Button cargoDetailCancelButton;

    @BindView(R.id.cargoDetailOrder)
    protected Button cargoDetailOrder;

    @OnClick(R.id.cargoDetailCancelButtonn)
    public void cargoDetailCancelButtonClick() {
        ((MapsActivity) getActivity()).backToMainMapView();
    }

    @OnClick(R.id.cargoDetailOrder)
    public void cargoDetailOrderClick() {
        CargoItem cargoItem = new CargoItem();
        cargoItem.setOrigin(new Point(currentRouteMarkerList.get(0).getPosition()));
        cargoItem.setDestination(new Point(currentRouteMarkerList.get(1).getPosition()));
        cargoItem = bindCargoFromForm(cargoItem);
        setUserIDOnItem(cargoItem);
        String key = cargoRef.push().getKey();
        cargoRef.child(key).setValue(cargoItem);

        ((MapsActivity) getActivity()).backToMainMapView();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.add_cargo_fragment, container, false);
        ButterKnife.bind(this, rootView);


        bindViewFromGeocoderData(currentRouteMarkerList.get(1).getPosition());

        return rootView;
    }


    public CargoItem bindCargoFromForm(CargoItem cargo) {

        cargo.setDestCountryCode(cargoDetailCountryEditText.getText().toString());
        cargo.setDestStreet(cargoDetailDestEditText.getText().toString());
        cargo.setDestZipCodeAndCity(cargoDetailZipCodeEditText.getText().toString());
        cargo.setPhoneNumber(cargoDetailPhoneNumberEditText.getText().toString());
        cargo.setAddidtionInfo(additionalDetailsEditText.getText().toString());

        cargo.setOffer(zeroIfEditTextIsEmpty(cargoDetailEstimateEditText));
        cargo.setHeight(zeroIfEditTextIsEmpty(cargoDetailHeightEditText));
        cargo.setLength(zeroIfEditTextIsEmpty(cargoDetailLengthEditText));
        cargo.setWeight(zeroIfEditTextIsEmpty(cargoDetailWeightEditText));
        cargo.setWidth(zeroIfEditTextIsEmpty(cargoDetailWidthEditText));

        return cargo;
    }

    private void bindViewFromGeocoderData(LatLng location) {
        Geocoder geocoder = new Geocoder(MapsActivity.mContext);
        Address address = null;
        try {
            address = geocoder.getFromLocation(location.latitude, location.longitude, 1).get(0);
            cargoDetailDestEditText.setText(address.getSubAdminArea());
            cargoDetailZipCodeEditText.setText(address.getPostalCode() + " " + address.getLocality());
            cargoDetailCountryEditText.setText(address.getCountryCode());


        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
