package com.cargocrew.cargoapp.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.cargocrew.cargoapp.R;
import com.cargocrew.cargoapp.activities.MapsActivity;
import com.cargocrew.cargoapp.models.CargoItem;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.cargocrew.cargoapp.Utils.showButtonForOwner;
import static com.cargocrew.cargoapp.activities.MapsActivity.BUNDLE_CARGO_ITEM;
import static com.cargocrew.cargoapp.activities.MapsActivity.BUNDLE_KEY;
import static com.cargocrew.cargoapp.activities.MapsActivity.BUNDLE_TRUCK_ITEM;
import static com.cargocrew.cargoapp.activities.MapsActivity.cargoRef;

/**
 * Created by Miki on 08.06.2017.
 */

public class CargoDetailFragment extends Fragment {

    @BindView(R.id.cargoDetailBar)
    protected LinearLayout cargoDetailBar;

    @BindView(R.id.cargoDetailDeliveryAddressTextView)
    protected TextView cargoDetailDeliveryAddressTextView;

    @BindView(R.id.cargoDetailLenghtTextView)
    protected TextView cargoDetailLengthTextView;

    @BindView(R.id.cargoDetailHeightTextView)
    protected TextView cargoDetailHeightTextView;

    @BindView(R.id.cargoDetailWidthTextView)
    protected TextView cargoDetailWidthTextView;

    @BindView(R.id.cargoDetailWeightTextView)
    protected TextView cargoDetailWeightTextView;

    @BindView(R.id.cargoDetailNoteTextView)
    protected TextView cargoDetailNoteTextView;

    @BindView(R.id.cargoDetailPhoneNumberTextView)
    protected TextView cargoDetailPhoneNumberTextView;

    @BindView(R.id.cargoDetailBudgetTextView)
    protected TextView cargoDetailBudgetTextView;

    @BindView(R.id.cargoDetailDeleteCargo)
    protected Button cargoDetailDeleteCargo;

    @BindView(R.id.cargoDetailRadioButton)
    protected RadioButton cargoDetailRadioButton;

    @OnClick(R.id.cargoDetailDeleteCargo)
    public void deleteCargoClick() {
        cargoRef.child(getArguments().getString(BUNDLE_KEY)).removeValue();
        ((MapsActivity) getActivity()).backToMainMapView();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.cargo_detail_fragment, container, false);
        ButterKnife.bind(this, rootView);

        CargoItem cargoItem = (CargoItem) getArguments().getSerializable(BUNDLE_CARGO_ITEM);
        bindViewFromCargo(cargoItem);
        showButtonForOwner(cargoItem,cargoDetailDeleteCargo);

        return rootView;
    }


    public void bindViewFromCargo(CargoItem cargo) {

        cargoDetailDeliveryAddressTextView.setText(cargo.getDestStreet() + "\n" + cargo.getDestZipCodeAndCity() + "; " + cargo.getDestCountryCode());
        cargoDetailLengthTextView.setText("L: " + String.valueOf(cargo.getLength()) + " cm");
        cargoDetailHeightTextView.setText("H: " + String.valueOf(cargo.getHeight()) + " cm");
        cargoDetailWidthTextView.setText("W: " + String.valueOf(cargo.getWidth()) + " cm");
        cargoDetailWeightTextView.setText("Weight: " + String.valueOf(cargo.getWeight()) + " kg");
        cargoDetailNoteTextView.setText(cargo.getAddidtionInfo());
        cargoDetailPhoneNumberTextView.setText("Contact number: " + cargo.getPhoneNumber());
        cargoDetailBudgetTextView.setText("Offer: " + String.valueOf(cargo.getOffer()));

        showButtonForOwner(cargo, cargoDetailDeleteCargo);
    }

}
