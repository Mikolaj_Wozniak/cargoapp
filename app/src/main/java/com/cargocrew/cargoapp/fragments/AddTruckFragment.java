package com.cargocrew.cargoapp.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.cargocrew.cargoapp.R;
import com.cargocrew.cargoapp.activities.MapsActivity;
import com.cargocrew.cargoapp.models.CargoItem;
import com.cargocrew.cargoapp.models.Point;
import com.cargocrew.cargoapp.models.TruckItem;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.cargocrew.cargoapp.Utils.setUserIDOnItem;
import static com.cargocrew.cargoapp.activities.MapsActivity.cargoRef;
import static com.cargocrew.cargoapp.activities.MapsActivity.currentRouteMarkerList;
import static com.cargocrew.cargoapp.activities.MapsActivity.truckRef;

/**
 * Created by Miki on 08.06.2017.
 */

public class AddTruckFragment extends Fragment {

    @BindView(R.id.addTruckBar)
    protected LinearLayout addTruckBar;

    @BindView(R.id.truckDetailDateEditText)
    protected EditText truckDetailDateEditText;

    @BindView(R.id.truckDetailTelEditText)
    protected EditText truckDetailTelEditText;

    @BindView(R.id.truckDetailTypeEditText)
    protected EditText truckDetailTypeEditText;

    @BindView(R.id.truckDetaiAddButton)
    protected Button truckDetailAddButton;

    @BindView(R.id.truckDetailCancelButton)
    protected Button truckDetailCancelButton;

    @OnClick(R.id.truckDetailCancelButton)
    public void truckDetailCancelButtonClick() {
        ((MapsActivity) getActivity()).backToMainMapView();
    }

    @OnClick(R.id.truckDetaiAddButton)
    public void truckDetaiAddButtonClick() {
        TruckItem truckItem = new TruckItem();
        truckItem.setOrigin(new Point(currentRouteMarkerList.get(0).getPosition()));
        truckItem.setDestination(new Point(currentRouteMarkerList.get(1).getPosition()));
        truckItem = bindTruckFromForm(truckItem);
        setUserIDOnItem(truckItem);
        String key = truckRef.push().getKey();
        truckRef.child(key).setValue(truckItem);

        ((MapsActivity) getActivity()).backToMainMapView();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.add_truck_fragment, container, false);
        ButterKnife.bind(this, rootView);


        return rootView;
    }

    public TruckItem bindTruckFromForm(TruckItem truckItem) {

        truckItem.setDestStreet(truckDetailDateEditText.getText().toString());
        truckItem.setPhoneNumber(truckDetailTelEditText.getText().toString());
        truckItem.setType(truckDetailTypeEditText.getText().toString());

        return truckItem;
    }
}
