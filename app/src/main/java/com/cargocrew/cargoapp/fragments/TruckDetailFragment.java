package com.cargocrew.cargoapp.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cargocrew.cargoapp.R;
import com.cargocrew.cargoapp.activities.MapsActivity;
import com.cargocrew.cargoapp.models.TruckItem;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.cargocrew.cargoapp.Utils.showButtonForOwner;
import static com.cargocrew.cargoapp.activities.MapsActivity.BUNDLE_KEY;
import static com.cargocrew.cargoapp.activities.MapsActivity.BUNDLE_TRUCK_ITEM;
import static com.cargocrew.cargoapp.activities.MapsActivity.truckRef;

/**
 * Created by Miki on 08.06.2017.
 */

public class TruckDetailFragment extends Fragment {

    @BindView(R.id.truckDetailBar)
    protected LinearLayout truckDetailBar;

    @BindView(R.id.truckDetailDateTextView)
    protected TextView truckDetailDateTextView;

    @BindView(R.id.truckDetailTelTextView)
    protected TextView truckDetailTelTextView;

    @BindView(R.id.truckDetailTypeTextView)
    protected TextView truckDetailTypeTextView;

    @BindView(R.id.truckDetaildeleteTruck)
    protected Button truckDetailDeleteTruck;

    @BindView(R.id.truckDetailDateTextViewWrapper)
    protected LinearLayout truckDetailDateTextViewWrapper;

    @BindView(R.id.truckDetailTelTextViewWrapper)
    protected LinearLayout truckDetailTelTextViewWrapper;

    @BindView(R.id.truckDetailTypeTextViewWrapper)
    protected LinearLayout truckDetailTypeTextViewWrapper;

    @OnClick(R.id.truckDetaildeleteTruck)
    public void deleteTruckClick() {
        truckRef.child(getArguments().getString(BUNDLE_KEY)).removeValue();
        ((MapsActivity) getActivity()).backToMainMapView();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.truck_detail_fragment, container, false);
        ButterKnife.bind(this, rootView);

        TruckItem truckItem = (TruckItem) getArguments().getSerializable(BUNDLE_TRUCK_ITEM);
        bindViewFromTruck(truckItem);
        showButtonForOwner(truckItem,truckDetailDeleteTruck);


        return rootView;
    }

    public void bindViewFromTruck(TruckItem truck) {
        if (truck.getDate() != null && !truck.getDate().isEmpty()) {
            truckDetailDateTextView.setText(truck.getDate());
        } else {
            truckDetailDateTextViewWrapper.setVisibility(View.GONE);
        }
        if (truck.getPhoneNumber() != null && !truck.getPhoneNumber().isEmpty()) {
            truckDetailTelTextView.setText(truck.getPhoneNumber());
        } else {
            truckDetailTelTextViewWrapper.setVisibility(View.GONE);
        }
        if (truck.getType() != null && !truck.getType().isEmpty()) {
            truckDetailTypeTextView.setText(truck.getType());
        } else {
            truckDetailTypeTextViewWrapper.setVisibility(View.GONE);
        }

        showButtonForOwner(truck, truckDetailDeleteTruck);
    }

}
