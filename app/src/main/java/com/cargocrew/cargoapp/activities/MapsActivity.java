package com.cargocrew.cargoapp.activities;

import android.Manifest;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.cargocrew.cargoapp.R;
import com.cargocrew.cargoapp.drawingroute.DownloadTask;
import com.cargocrew.cargoapp.drawingroute.Services;
import com.cargocrew.cargoapp.fragments.AddCargoFragment;
import com.cargocrew.cargoapp.fragments.AddTruckFragment;
import com.cargocrew.cargoapp.fragments.CargoDetailFragment;
import com.cargocrew.cargoapp.fragments.TruckDetailFragment;
import com.cargocrew.cargoapp.models.CargoItem;
import com.cargocrew.cargoapp.models.TransportationItem;
import com.cargocrew.cargoapp.models.TruckItem;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.cargocrew.cargoapp.R.id.map;
import static com.cargocrew.cargoapp.Utils.isOnline;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    public static final int REQUEST_ACCESS_FINE_LOCATION = 1001;


    private final int MAP_ONCLICK_NULL = 100;

    private final int MAP_ONCLICK_ZOOM_OUT_FROM_DETAIL = 101;

    private final int MAP_ONCLICK_ADD_MARKER = 102;


    private final int MARKER_ONCLICK_NULL = 200;

    private final int MARKER_ONCLICK_ZOOM_FORM_DETAIL = 201;


    private final int TRUCK_DETAIL_FRAGMENT = 301;

    private final int CARGO_DETAIL_FRAGMENT = 302;

    private final int ADD_TRUCK_FRAGMENT = 303;

    private final int ADD_CARGO_FRAGMENT = 304;

    
    public static final String BUNDLE_CARGO_ITEM = "Cargo Item";

    public static final String BUNDLE_TRUCK_ITEM = "Truck Item";

    public static final String BUNDLE_KEY = "Key";


    public static FirebaseDatabase database = FirebaseDatabase.getInstance();

    public static DatabaseReference cargoRef = database.getInstance().getReference("Cargo");

    public static DatabaseReference truckRef = database.getInstance().getReference("Truck");


    private int mapOnClickState = MAP_ONCLICK_NULL;

    private int markerOnClickState = MARKER_ONCLICK_ZOOM_FORM_DETAIL;


    public static Context mContext;

    public static GoogleMap mMap;



    public static List<Marker> currentRouteMarkerList = new ArrayList<>();


    private HashMap<String, TransportationItem> cargoHashMap = new HashMap<>();

    private HashMap<String, TransportationItem> truckHashMap = new HashMap<>();

    private HashMap<String, TransportationItem> currentSelect = new HashMap<>();


    private String selectedMarker = null;

    private CameraPosition cameraPosition = null;

    public static boolean mapRefreshable = true;


    @BindView(R.id.searchEditTextWrapper)
    LinearLayout searchEditTextWrapper;

    @BindView(R.id.searchEditText)
    EditText searchEditText;

    @BindView(R.id.ButtonSearch)
    Button ButtonSearch;

    @BindView(R.id.cancelButtonOnSearch)
    Button cancelButtonOnSearch;

    @BindView(R.id.floatingActionButtonOpenSearch)
    FloatingActionButton floatingActionButtonOpenSearch;

    @BindView(R.id.floatingActionButtonSwitch)
    FloatingActionButton floatingActionButtonSwitch;

    @BindView(R.id.floatingLoginOptionsButton)
    FloatingActionButton floatingLoginOptionsButton;

    @BindView(R.id.fragmentPlaceHolder)
    LinearLayout fragmentPlaceHolder;


    @OnClick(R.id.floatingActionButtonOpenSearch)
    public void floatingActionButtonOpenSearchClick() {
        if (isOnline(mContext)) {
            mMap.clear();
            hideFloatingButtons();
            searchEditTextWrapper.setVisibility(View.VISIBLE);
            mapOnClickState = MAP_ONCLICK_ADD_MARKER;
        } else {
            Toast.makeText(this, R.string.Internet_connection_require, Toast.LENGTH_SHORT).show();
        }

    }

    @OnClick(R.id.floatingActionButtonSwitch)
    public void floatingActionButtonSwitchClick() {
        if (currentSelect == cargoHashMap) {
            currentSelect = truckHashMap;
            floatingActionButtonSwitch.setImageDrawable(getResources().getDrawable(R.drawable.truck));
        } else {
            currentSelect = cargoHashMap;
            floatingActionButtonSwitch.setImageDrawable(getResources().getDrawable(R.drawable.cargo_icon));
        }
        mMap.clear();
        drawTransportationMarkers(currentSelect);
    }

    @OnClick(R.id.floatingLoginOptionsButton)
    public void floatingLoginOptionsButtonClick() {
        startActivity(new Intent(MapsActivity.this, LoginOptionsActivity.class));
    }

    @OnClick(R.id.ButtonSearch)
    public void search() {
        String location = searchEditText.getText().toString();
        if (location != null && !location.equals("")) {
            LatLng coordinationAsLatLng = getCoordinationFromName(location);
            if (coordinationAsLatLng != null) {
                getRoute(coordinationAsLatLng);
            } else {
                Toast.makeText(this, R.string.Couldn_t_find_location,Toast.LENGTH_SHORT).show();
                searchEditText.setText("");
            }
        }
    }

    @OnClick(R.id.cancelButtonOnSearch)
    public void cancelSearch() {
        mMap.animateCamera(CameraUpdateFactory.zoomTo(5f));
        backToMainMapView();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);
        currentSelect = cargoHashMap;
        haveLocationPermission();
        setUpMap();
        setUpDataBase();
    }

    private boolean haveLocationPermission() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_ACCESS_FINE_LOCATION);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_ACCESS_FINE_LOCATION && grantResults[0] != -1) {
            {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                //TODO
                mMap.setMyLocationEnabled(true);
            }
        }
    }

    private void setUpMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mContext = getBaseContext();
        mMap = googleMap;
        mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.map));
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        LatLng polandCenter = new LatLng(52.069381, 19.480334);
        cameraPosition = CameraPosition.builder().zoom(5.4f).target(polandCenter).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        }

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(com.google.android.gms.maps.model.LatLng latLng) {

                switch (mapOnClickState) {
                    case MAP_ONCLICK_ADD_MARKER:
                        getRoute(latLng);
                        break;
                    case MAP_ONCLICK_ZOOM_OUT_FROM_DETAIL:
                        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        backToMainMapView();
                        break;
                    default:
                        break;
                }

            }
        });
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

            @Override
            public boolean onMarkerClick(Marker arg0) {

                switch (markerOnClickState) {
                    case MARKER_ONCLICK_ZOOM_FORM_DETAIL:

                        mapRefreshable = false;
                        markerOnClickState = MARKER_ONCLICK_NULL;

                        hideFloatingButtons();

                        selectedMarker = (String) arg0.getTag();

                        TransportationItem selectedTransportItem = null;
                        if (cargoHashMap.containsKey(selectedMarker)) {
                            selectedTransportItem = cargoHashMap.get(selectedMarker);
                        }
                        if (truckHashMap.containsKey(selectedMarker)) {
                            selectedTransportItem = truckHashMap.get(selectedMarker);
                        }

                        LatLng origin = selectedTransportItem.getOrigin().toLatLong();
                        LatLng dest = selectedTransportItem.getDestination().toLatLong();
                        Services services = new Services();
                        String url = services.getDirectionsUrl(origin, dest);
                        DownloadTask downloadTask = new DownloadTask();

                        mMap.clear();

                        Drawable drawable = getResources().getDrawable(R.drawable.pickup_piont);
                        if (currentSelect == truckHashMap) {
                            drawable = getResources().getDrawable(R.drawable.truck_map_icno);
                        }

                        BitmapDescriptor markerStartIcon = getBitmapDescriptor(drawable);
                        BitmapDescriptor markerFinishIcon = getBitmapDescriptor(getResources().getDrawable(R.drawable.fishish_flag));

                        Marker startMarker = mMap.addMarker(new MarkerOptions()
                                .position(origin).icon(markerStartIcon));
                        Marker destMarker = mMap.addMarker(new MarkerOptions()
                                .position(dest).icon(markerFinishIcon));

                        if (isOnline(mContext)) {
                            downloadTask.execute(url);
                        }

                        ArrayList<Marker> markers = new ArrayList<>();
                        markers.add(startMarker);
                        markers.add(destMarker);

                        if (selectedTransportItem.getClass() == CargoItem.class) {
                            swapFragment(CARGO_DETAIL_FRAGMENT, selectedMarker);
                        }
                        if (selectedTransportItem.getClass() == TruckItem.class) {
                            swapFragment(TRUCK_DETAIL_FRAGMENT, selectedMarker);
                        }
                        cameraPosition = mMap.getCameraPosition();

                        settingZoomAroundMarkerList(markers);
                        mapOnClickState = MAP_ONCLICK_ZOOM_OUT_FROM_DETAIL;
                        break;
                    default:
                        break;
                }

                return true;
            }
        });
    }

    public void setUpDataBase() {

        cargoRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                String key = dataSnapshot.getKey();
                CargoItem cargoItem = dataSnapshot.getValue(CargoItem.class);
                cargoHashMap.put(key, cargoItem);

                if (mapRefreshable && currentSelect == cargoHashMap) {
                    mMap.clear();
                    drawTransportationMarkers(currentSelect);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

                String key = dataSnapshot.getKey();
                if (cargoHashMap.containsKey(key)) {
                    cargoHashMap.remove(key);
                }

                if (mapRefreshable && currentSelect == cargoHashMap) {
                    mMap.clear();
                    drawTransportationMarkers(currentSelect);
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });


        truckRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                String key = dataSnapshot.getKey();
                TruckItem truckItem = dataSnapshot.getValue(TruckItem.class);
                truckHashMap.put(key, truckItem);

                if (mapRefreshable && currentSelect == truckHashMap) {
                    mMap.clear();
                    drawTransportationMarkers(currentSelect);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

                String key = dataSnapshot.getKey();
                if (truckHashMap.containsKey(key)) {
                    truckHashMap.remove(key);
                }

                if (mapRefreshable && currentSelect == truckHashMap) {
                    mMap.clear();
                    drawTransportationMarkers(currentSelect);
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }

    private LatLng getCoordinationFromName(String location) {
        Geocoder geocoder = new Geocoder(this);
        Address address = null;
        try {
            address = geocoder.getFromLocationName(location, 1).get(0);
            LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
            return latLng;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void getRoute(LatLng coordination) {

        mapRefreshable = false;

        if (currentRouteMarkerList.size() == 0) {

            mMap.clear();

            currentRouteMarkerList.add(mMap.addMarker(new MarkerOptions()
                    .position(coordination)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN))));
            searchEditText.setText("");
            settingZoomAroundMarkerList(currentRouteMarkerList);

            searchEditText.setHint(R.string.Enter_destination);


        } else if (currentRouteMarkerList.size() == 1) {

            currentRouteMarkerList.add(mMap.addMarker(new MarkerOptions()
                    .position(coordination)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))));

            if (currentSelect == cargoHashMap)
                swapFragment(ADD_CARGO_FRAGMENT, null);
            if (currentSelect == truckHashMap)
                swapFragment(ADD_TRUCK_FRAGMENT, null);

            LatLng origin = currentRouteMarkerList.get(0).getPosition();
            LatLng dest = currentRouteMarkerList.get(1).getPosition();
            Services s = new Services();
            String url = s.getDirectionsUrl(origin, dest);
            DownloadTask downloadTask = new DownloadTask();
            if (isOnline(mContext)) {
                downloadTask.execute(url);
            }

            settingZoomAroundMarkerList(currentRouteMarkerList);

            searchEditTextWrapper.setVisibility(View.GONE);
            mapOnClickState = MAP_ONCLICK_NULL;

        } else {
            Log.i("M", "getRoute error");
            currentRouteMarkerList.clear();
        }

    }

    private void settingZoomAroundMarkerList(List<Marker> markerList) {

        if (markerList.size() >= 2) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (Marker marker : markerList) {
                builder.include(marker.getPosition());
            }
            LatLngBounds bounds = builder.build();
            int padding = 100;
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            mMap.animateCamera(cu);
        } else {
            LatLngBounds bounds = this.mMap.getProjection().getVisibleRegion().latLngBounds;
            if (!bounds.contains(markerList.get(0).getPosition())) {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(markerList.get(0).getPosition(), 8));
            }
        }
    }

    public void drawTransportationMarkers(HashMap<String, TransportationItem> transportationItemHashMap) {

        Drawable drawable = getResources().getDrawable(R.drawable.truck_map_icno);
        if (currentSelect == cargoHashMap)
            drawable = getResources().getDrawable(R.drawable.cargo_map_icon);

        BitmapDescriptor markerIcon = getBitmapDescriptor(drawable);

        for (Map.Entry<String, TransportationItem> entry : transportationItemHashMap.entrySet()) {
            Marker marker = mMap.addMarker(new MarkerOptions()
                    .position(entry.getValue().getOrigin().toLatLong())
                    .icon(markerIcon));
            marker.setTag(entry.getKey());
        }

    }

    private BitmapDescriptor getBitmapDescriptor(Drawable drawable) {

        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    public void backToMainMapView() {

        fragmentPlaceHolder.setVisibility(View.GONE);
        searchEditTextWrapper.setVisibility(View.GONE);
        searchEditText.setText("");
        searchEditText.setHint(R.string.Enter_start_location);
        showFloatingButtons();

        mapRefreshable = true;

        mapOnClickState = MAP_ONCLICK_NULL;
        markerOnClickState = MARKER_ONCLICK_ZOOM_FORM_DETAIL;

        mMap.clear();
        drawTransportationMarkers(currentSelect);

        currentRouteMarkerList.clear();
        selectedMarker = null;

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    private void hideFloatingButtons() {
        floatingActionButtonOpenSearch.setVisibility(View.GONE);
        floatingLoginOptionsButton.setVisibility(View.GONE);
        floatingActionButtonSwitch.setVisibility(View.GONE);
    }

    private void showFloatingButtons() {
        floatingActionButtonOpenSearch.setVisibility(View.VISIBLE);
        floatingLoginOptionsButton.setVisibility(View.VISIBLE);
        floatingActionButtonSwitch.setVisibility(View.VISIBLE);
    }

    private void swapFragment(int fragmentPick, String key) {
        Bundle bundle = new Bundle();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        switch (fragmentPick) {
            case TRUCK_DETAIL_FRAGMENT:
                TruckDetailFragment truckDetailFragment = new TruckDetailFragment();
                bundle.putSerializable(BUNDLE_TRUCK_ITEM, truckHashMap.get(key));
                bundle.putString(BUNDLE_KEY, key);
                truckDetailFragment.setArguments(bundle);
                transaction.replace(R.id.fragmentPlaceHolder, truckDetailFragment);
                fragmentPlaceHolder.setVisibility(View.VISIBLE);
                break;
            case CARGO_DETAIL_FRAGMENT:
                CargoDetailFragment cargoDetailFragment = new CargoDetailFragment();
                bundle.putSerializable(BUNDLE_CARGO_ITEM, cargoHashMap.get(key));
                bundle.putString(BUNDLE_KEY, key);
                cargoDetailFragment.setArguments(bundle);
                transaction.replace(R.id.fragmentPlaceHolder, cargoDetailFragment);
                fragmentPlaceHolder.setVisibility(View.VISIBLE);
                break;
            case ADD_TRUCK_FRAGMENT:
                AddTruckFragment addTruckFragment = new AddTruckFragment();
                addTruckFragment.setArguments(bundle);
                transaction.replace(R.id.fragmentPlaceHolder, addTruckFragment);
                fragmentPlaceHolder.setVisibility(View.VISIBLE);
                break;
            case ADD_CARGO_FRAGMENT:
                AddCargoFragment addCargoFragment = new AddCargoFragment();
                addCargoFragment.setArguments(bundle);
                transaction.replace(R.id.fragmentPlaceHolder, addCargoFragment);
                fragmentPlaceHolder.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
        transaction.commit();
    }
}